from flask_assets import Environment, Bundle
from app import app

bundles = {

    'base_js': Bundle(
        'js/base.js', 
        filters='jsmin',
        output='gen/base/base.js'),

    'base_css': Bundle(
        'css/base.css',
        filters='cssmin',
        output='gen/base/base.css'),

    'main_js': Bundle(
        'js/main.js',
        filters='jsmin',
        output='gen/main.js'),

    'style_css': Bundle(
        'css/style.css',
        filters='cssmin',
        output='gen/style.css')

}

assets = Environment(app)
assets.register(bundles)
