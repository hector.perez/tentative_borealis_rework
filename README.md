# Solaires E-Commerce Site

A Python3 implementation of an ecommerce website to market the Solaires products. This front end connects to a PostgreSQL database via a Node.js API. Both The ecommerce site and API are hosted on Heroku. Refer to the Gitlab Wiki for project setup or below to get started.

### Prerequisites

- [Git](https://git-scm.com)
- [Python 3.7.0](https://www.python.org/)
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
- [NPM](https://www.npmjs.com/)
- [PostgreSQL](https://www.postgresql.org/)

### macOS and Linux

***

#### Website

1. Clone the e-commerce to your local drive.

    ```bash
    git clone https://gitlab.com/solaires/solaires-flask.git  'solaires-flask'
    ```

2. Open your local repository create a virtual environment.

    ```bash
    cd 'solaires-flask' && python3 -m venv venv
    ```

3. Activate your newly created virtual environment.

    ```bash
    source venv/bin/activate
    ```

4. Once in your virtual environment, install the dependencies using pip and the provided requirements.txt.

    ```bash
    pip3 install -r requirements.txt
    ```

5. Lastly, configure the environment variables and run the program.

    ```bash
    export FLASK_APP=solaires.py
    ```

    ```bash
    export FLASK_DEBUG=1
    ```

    ```bash
    flask run
    ```


---

For windows : 

The commands order is the same, but they are a little different . 

To start your virtual environment after creation , 

call venv/scripts/activate

Then you need to set your environment variables 

set FLASK_APP=solaires.py

set FLASK_DEBUG=1

flask run 

--- 

To stop the server from running you can do CTRL+C




